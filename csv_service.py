import pandas as pd
from pathlib import Path

from pandas import DataFrame

COLUMNS = ["id", "arrival", "departure", "success"]

SUCCESS_DIFF = 180

default_file_path = Path(Path(__file__).parent, "flights.csv")

MAX_SUCCESS = 20


class Counter:
    def __init__(self):
        self.count = 0

    def increase(self):
        self.count += 1

    def is_achieved(self):
        return self.count >= MAX_SUCCESS


def is_success(csv_row, counter: Counter):
    if counter.is_achieved():
        return False
    diff = csv_row.dt_departure - csv_row.dt_arrival
    diff_minutes = diff.seconds / 60
    success_by_diff = diff_minutes > SUCCESS_DIFF
    if success_by_diff:
        counter.increase()
    return success_by_diff


def read_csv(file_path):
    df = pd.read_csv(file_path, names=COLUMNS,
                     converters={'arrival': str.strip, 'departure': str.strip})
    return df


def calculate_status(df):
    df["dt_departure"] = pd.to_datetime(df["departure"], format="%H:%M")
    df["dt_arrival"] = pd.to_datetime(df["arrival"])
    df.sort_values(by=['dt_arrival'], inplace=True)
    counter = Counter()
    df["success"] = df.apply(is_success, axis=1, args=(counter,))
    df.drop(columns=["dt_arrival", "dt_departure"], inplace=True)


def update_csv(file_path: Path = default_file_path, df: DataFrame = None):
    if df is None:
        df = read_csv(file_path)
    calculate_status(df)
    df = df[COLUMNS]
    df.to_csv(file_path, index=False, header=False)


def get_row_by_id(flight_id, file_path: Path = default_file_path):
    with open(file_path, 'r') as f:
        for line in f.readlines():
            if line.startswith(f"{flight_id},"):
                line = line.rstrip("\n")
                return line


def validate_row(row: dict):
    assert sorted(row.keys()) == sorted(COLUMNS)
    #   validate values ...


def create_or_update_row(row: dict, file_path: Path = default_file_path):
    row["success"] = False  # fixed value, its recalculate later
    validate_row(row)
    df = read_csv(file_path)
    updated_row = DataFrame(data=row, index=[0])
    df = pd.concat([df, updated_row], sort=True).drop_duplicates("id", keep='last')
    # Recalculate status and update csv file
    update_csv(file_path, df)
