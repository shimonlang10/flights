import json
import logging

from flask import Flask, abort, request

from csv_service import get_row_by_id, COLUMNS, create_or_update_row

BASE_API = "/api/v0"

log = logging.getLogger(__name__)

app = Flask(__name__)


@app.get(f"{BASE_API}/flight/<flight_id>/")
def get_row(flight_id):
    row = get_row_by_id(flight_id)
    if row:
        dict_row = zip(COLUMNS, row.split(","))
        return dict(dict_row)
    else:
        abort(404)


@app.post(f"{BASE_API}/flight/")
def post_row():
    row = json.loads(request.data)
    print(row)
    try:

        create_or_update_row(row)
        row = get_row_by_id(row["id"])
        dict_row = zip(COLUMNS, row.split(","))
        return dict(dict_row)
    except Exception as e:
        log.error(e)
        abort(400)


if __name__ == "__main__":
    app.run(debug=True)