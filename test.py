from datetime import datetime
from api import app as _app
import pytest


@pytest.fixture()
def app():
    app = _app
    app.config.update({
        "TESTING": True,
    })

    # other setup can go here

    yield app


#     # clean up / reset resources here
from api import BASE_API


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


def test_new_row(client):
    data = {
        "arrival": "08:30",
        "departure": "12:00",
        "id": f"G86_{datetime.now()}"}

    # test insert new row
    response = client.post(f"{BASE_API}/flight/", json=data)
    response_json = response.json
    response_json.pop("success")
    assert response_json == data

    # test get row
    response = client.get(f"{BASE_API}/flight/{data['id']}/", query_string={"flight_id": data["id"]})
    response_json = response.json
    response_json.pop("success")
    assert response_json == data

    # test update row
    data["arrival"] = "11:30"
    response = client.post(f"{BASE_API}/flight/", json=data)
    response_json = response.json
    response_json.pop("success")
    assert response_json == data

    # test get row
    response = client.get(f"{BASE_API}/flight/{data['id']}/", query_string={"flight_id": data["id"]})
    response_json = response.json
    response_json.pop("success")
    assert response_json == data
